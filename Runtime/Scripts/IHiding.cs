﻿using Cysharp.Threading.Tasks;

namespace UIWindowExtenject
{
    /// <summary>
    /// Interface for Hide Window for all Window with param
    /// </summary>
    public interface IHiding
    {
        /// <summary>
        /// Hide Window
        /// </summary>
        UniTask Hide();
    }
}
