﻿using UnityEngine;
using Zenject;

namespace UIWindowExtenject {
    /// <summary>
    /// Window Instaler for all windows
    /// </summary>
    public class WindowInstaller : MonoInstaller {

        [SerializeField]
        private WindowsSettings _windowsSettings;

        public override void InstallBindings() {
            Container.Bind<WindowsSettings>().FromInstance(_windowsSettings);
            Container.BindInterfacesAndSelfTo<PoolController>().AsSingle();
            Container.BindInterfacesAndSelfTo<WindowController>().AsSingle();
        }
    }
}
