using Cysharp.Threading.Tasks;
using UnityEngine;

namespace UIWindowExtenject 
{

    /// <summary>
    /// Basic Panel
    /// </summary>
    public class Panel : MonoBehaviour, IShow, IHide 
    {
        /// <summary>
        /// Hide Window
        /// </summary>
        public void Hide() 
        {
            gameObject.SetActive(false);
        }
        /// <summary>
        /// Show Window
        /// </summary>

        public void Show(IParameters parameters) 
        {
            gameObject.SetActive(true);
        }
    }
}
