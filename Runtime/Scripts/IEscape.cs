namespace UIWindowExtenject
{

    /// <summary>
    /// Escape from window
    /// </summary>
    public interface IEscape {
        /// <summary>
        /// Escape
        /// </summary>
        void Escape();
    }
}
