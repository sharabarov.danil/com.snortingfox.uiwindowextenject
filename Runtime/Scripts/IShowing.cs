﻿using Cysharp.Threading.Tasks;

namespace UIWindowExtenject
{
    /// <summary>
    /// Interface for Show Window for all Window with param
    /// </summary>
    public interface IShowing {
        /// <summary>
        /// Show Window
        /// </summary>
        UniTask Show(IParameters parameters = null);
    }
}
