using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace UIWindowExtenject
{

    public class StartPopupSequencer : MonoBehaviour
    {
        [Header("Window Function")]
        [SerializeField]
        protected WindowData[] _windowDatas;

        protected WindowController _windowController;

        [Inject]
        public void Construct(WindowController windowController)
        {
            _windowController = windowController;
        }

        private async void Start()
        {
            foreach (var windowData in _windowDatas)
            {
                await UniTask.NextFrame();
                await _windowController.OpenSequencePopup(windowData);
            }
        }
    }
}
