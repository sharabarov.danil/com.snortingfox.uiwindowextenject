using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct WindowsSettings
{
    public int PoolCount;
    public Transform Parent;
}
