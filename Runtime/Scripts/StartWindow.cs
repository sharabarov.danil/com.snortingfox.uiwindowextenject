﻿using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace UIWindowExtenject {
    /// <summary>
    /// Call Window on Start
    /// </summary>
    public class StartWindow : MonoBehaviour {

        [Header("Window Function")]
        [SerializeField]
        protected WindowData _windowData;

        protected WindowController _windowController;

        [Inject]
        public void Construct(WindowController windowController)
        {
            _windowController = windowController;
        }

        private async void Start() 
        {
            await UniTask.NextFrame();
            await _windowController.OpenWindow(_windowData);
        }
    }
}
