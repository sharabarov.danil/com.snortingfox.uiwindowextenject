﻿namespace UIWindowExtenject
{
    /// <summary>
    /// Enter from window
    /// </summary>
    public interface IEnter
    {
        /// <summary>
        /// Enter
        /// </summary>
        void Enter();
    }
}
