﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Zenject;

namespace UIWindowExtenject {
    /// <summary>
    /// Controller for pool methods and properties
    /// </summary>
    public class PoolController 
    {
        private WindowsSettings _windowsSettings;
        private DiContainer _diContainer;

        private GameObject _currentWindowElement;
        private GameObject _previousWindowElement;
        private IEscape[] _escapeWindows = null;
        private IEnter[] _enterWindows = null;

        private Dictionary<string, GameObject> _cachedObject = new();

        public PoolController(WindowsSettings windowsSettings, DiContainer diContainer) 
        {
            _diContainer = diContainer;
            _windowsSettings = windowsSettings;
        }

        private bool Contain(WindowData windowData) 
        {
            return _cachedObject.ContainsKey(windowData.ID);
        }

        public bool IsActive(WindowData windowData) 
        {
            if (Contain(windowData)) 
            {
                return Element(windowData).activeInHierarchy;
            }
            return false;
        }

        private GameObject Element(WindowData windowData) 
        {
            return _cachedObject[windowData.ID];
        }

        private async UniTask<GameObject> Load(WindowData windowData) 
        {
            if (!Contain(windowData)) 
            {
                var handle = Addressables.LoadAssetAsync<GameObject>(windowData.Asset.AssetGUID);
                await handle.Task;

                if (handle.Status == AsyncOperationStatus.Succeeded) 
                {
                    GameObject tempWindow = _diContainer.InstantiatePrefab(handle.Task.Result);
                    _cachedObject.Add(windowData.ID, tempWindow);
                    if (_cachedObject.Count >= _windowsSettings.PoolCount) 
                    {

                        var notUsedElements = _cachedObject.Where(x => !x.Value.activeInHierarchy).Select(y=>y.Key).ToList();

                        foreach (var key in notUsedElements) 
                        {
                            GameObject.Destroy(_cachedObject[key]);
                            _cachedObject.Remove(key);
                        }
                    }
                }
            }

            GameObject tempAsset = Element(windowData);
            return tempAsset;
        }

        public void Release(WindowData windowData) 
        {
            if (Contain(windowData))
            {
                Addressables.Release(windowData.Asset);
            }
        }

        public async UniTask DeactivateAllPoolElements() 
        {
            foreach (var x in _cachedObject)
            {
                IHide[] _hideWindows = x.Value.GetComponentsInChildren<IHide>();
                if (_hideWindows != null)
                {
                    _hideWindows.ToList().ForEach(x=>x.Hide());
                }

                IHiding[] _hidingWindows = x.Value.GetComponentsInChildren<IHiding>();
                if (_hidingWindows != null) 
                {
                    var tasks = _hidingWindows.Select(x => x.Hide()).ToArray();
                    await UniTask.WhenAll(tasks);
                }
            }

        }

        /// <summary>
        /// Show Pool Element
        /// </summary>
        public async UniTask<GameObject> Show(WindowData windowData, IParameters parameters = null) 
        {
            Debug.Log($"Show WindowData with id - {windowData.ID}");
            GameObject tempWindow = await Load(windowData);
            tempWindow.transform.SetParent(_windowsSettings.Parent);

            _previousWindowElement = _currentWindowElement;
            _currentWindowElement = tempWindow;
            _escapeWindows = _currentWindowElement.GetComponentsInChildren<IEscape>();
            _enterWindows = _currentWindowElement.GetComponentsInChildren<IEnter>();

            IShow[] _showWindows = _currentWindowElement.GetComponentsInChildren<IShow>();

            if (_showWindows != null)
            {
                _showWindows.ToList().ForEach(x => x.Show(parameters));
            }

            IShowing[] _showingWindows = _currentWindowElement.GetComponentsInChildren<IShowing>();
            if (_showingWindows != null) 
            {
                var tasks = _showingWindows.Select(x => x.Show(parameters)).ToArray();
                await UniTask.WhenAll(tasks);
            }

            return _currentWindowElement;
        }

        /// <summary>
        /// Hide Pool Element
        /// </summary>
        public async UniTask Hide(WindowData windowData) 
        {
            if (Contain(windowData)) 
            {
                Debug.Log($"Hide WindowData with id - {windowData.ID}");
                _currentWindowElement = _previousWindowElement;

                if (_currentWindowElement != null)
                {
                    _escapeWindows = _currentWindowElement.GetComponents<IEscape>();
                    _enterWindows = _currentWindowElement.GetComponents<IEnter>();
                } 
                else 
                {
                    _escapeWindows = null;
                    _enterWindows = null;
                }

                GameObject tempAsset = Element(windowData);
                IHide[] _hideWindows = tempAsset.GetComponentsInChildren<IHide>();
                if (_hideWindows != null)
                {
                    _hideWindows.ToList().ForEach(x => x.Hide());
                }

                IHiding[] _hidingWindows = tempAsset.GetComponentsInChildren<IHiding>();
                if (_hidingWindows != null)
                {
                    var tasks = _hidingWindows.Select(x => x.Hide()).ToArray();
                    await UniTask.WhenAll(tasks);
                }
            }

        }

        /// <summary>
        /// Escape Last Element
        /// </summary>
        public void Escape() 
        {
            if (_escapeWindows != null && _escapeWindows.Length > 0) 
            {
                _escapeWindows.ToList().ForEach(x => x.Escape());
            }
        }

        /// <summary>
        /// Enter Last Element
        /// </summary>
        public void Enter()
        {
            if (_enterWindows != null && _enterWindows.Length > 0)
            {
                _enterWindows.ToList().ForEach(x => x.Enter());
            }
        }
    }
}
