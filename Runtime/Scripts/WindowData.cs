using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace UIWindowExtenject
{

    [CreateAssetMenu(fileName = "WindowData", menuName = "UIWindowExtenject/New WindowData")]
    public class WindowData : ScriptableObject
    {
        [Header("Window ID")]
        [SerializeField]
        public string _id;
        public string ID => _id;

        [Header("Asset Ref")]
        [SerializeField]
        private AssetReference _asset;
        public AssetReference Asset => _asset;
    }
}
