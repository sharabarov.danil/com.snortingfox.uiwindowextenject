﻿using UnityEngine;
using UnityEngine.Events;

namespace UIWindowExtenject
{
    /// <summary>
    /// Enter Action
    /// </summary>
    public class EnterAction : MonoBehaviour, IEnter
    {

        [Header("Event on Enter")]
        [SerializeField]
        private UnityEvent onEnter;

        /// <summary>
        /// Enter
        /// </summary>
        public void Enter()
        {
            onEnter?.Invoke();
        }

    }
}
