﻿namespace UIWindowExtenject
{
    public struct WindowDataWithParams
    {
        public WindowData WindowData { get; private set; }
        public IParameters Parameters { get; private set; }

        public WindowDataWithParams(WindowData windowData, IParameters parameters)
        {
            WindowData = windowData;
            Parameters = parameters;
        }

    }
}
