﻿using Cysharp.Threading.Tasks;

namespace UIWindowExtenject
{
    /// <summary>
    /// Interface for Hide Window for all Window with param
    /// </summary>
    public interface IHide
    {
        /// <summary>
        /// Hide Window
        /// </summary>
        void Hide();
    }
}
