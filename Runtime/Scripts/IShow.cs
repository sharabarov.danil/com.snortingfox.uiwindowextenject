﻿using Cysharp.Threading.Tasks;

namespace UIWindowExtenject
{
    /// <summary>
    /// Interface for Show Window for all Window with param
    /// </summary>
    public interface IShow {
        /// <summary>
        /// Show Window
        /// </summary>
        void Show(IParameters parameters = null);
    }
}
