﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace UIWindowExtenject
{
    /// <summary>
    /// Controller for windows
    /// </summary>
    public class WindowController : ITickable 
    {
        private PoolController _poolController;

        public WindowController(PoolController poolController) 
        {
            _poolController = poolController;
        }

        private List<WindowDataWithParams> _queueDatas = new();
        private List<string> _currentPopupList = new();

        /// <summary>
        /// Open window
        /// </summary>
        /// <param name="windowData"></param>
        /// <param name="parameters"></param>
        public async UniTask OpenWindow(WindowData windowData, IParameters parameters = null) 
        {
            if (windowData == null)
            {
                Debug.LogError("id is null or empty");
                return;
            }

            _queueDatas.Clear();
            _currentPopupList.Clear();

            await _poolController.DeactivateAllPoolElements();

            await UniTask.NextFrame();

            await _poolController.Show(windowData, parameters);
        }

        /// <summary>
        /// Open popup
        /// </summary>
        /// <param name="windowData"></param>
        /// <param name="parameters"></param>
        public async UniTask OpenPopup(WindowData windowData, IParameters parameters = null) 
        {
            if (windowData == null) 
            {
                Debug.LogError("id is null or empty");
                return;
            }

            if (IsActive(windowData)) 
            {
                Debug.LogError($"Window with the {windowData.ID} is already active");
                return;
            }

            _currentPopupList.Add(windowData.ID);
            await _poolController.Show(windowData, parameters);
        }

        /// <summary>
        /// Open popup
        /// </summary>
        /// <param name="windowData"></param>
        /// <param name="parameters"></param>
        public async UniTask OpenSequencePopup(WindowData windowData, IParameters parameters = null)
        {
            if (windowData == null)
            {
                Debug.LogError("id is null or empty");
                return;
            }

            if (IsActive(windowData) || _queueDatas.FirstOrDefault(x=>x.WindowData.ID == windowData.ID).Equals(default))
            {
                Debug.LogError($"Window with the {windowData.ID} is already active or in the queue");
                return;
            }

            WindowDataWithParams windowDataWithParams = new(windowData, parameters);

            if (_queueDatas.Count == 0 && _currentPopupList.Count == 0)
            {
                _currentPopupList.Add(windowData.ID);
                _queueDatas.Add(windowDataWithParams);
                await _poolController.Show(windowData, parameters);
            } 
            else 
            {
                _queueDatas.Add(windowDataWithParams);
            }
        }

        /// <summary>
        /// Close window/popup
        /// </summary>
        /// <param name="windowData"></param>
        public async UniTask ClosePopup(WindowData windowData)
        {
            if (windowData == null)
            {
                Debug.LogError("id is null or empty");
                return;
            }

            _currentPopupList.Remove(windowData.ID);
            await _poolController.Hide(windowData);

            await UniTask.NextFrame();

            if (_queueDatas.Count > 0)
            {
                var currentElement = _queueDatas[0];
                if (!_currentPopupList.Contains(currentElement.WindowData.ID))
                {
                    _currentPopupList.Add(currentElement.WindowData.ID);
                    await _poolController.Show(currentElement.WindowData, currentElement.Parameters);
                }
            }
        }

        /// <summary>
        /// Close sequence popup
        /// </summary>
        /// <param name="windowData"></param>
        public async UniTask CloseSequencePopup()
        {
            if (_queueDatas.Count > 0)
            {
                var currentElement = _queueDatas[0];
                _queueDatas.RemoveAt(0);
                _currentPopupList.Remove(currentElement.WindowData.ID);
                await _poolController.Hide(currentElement.WindowData);
            }

            await UniTask.NextFrame();

            if (_queueDatas.Count > 0 && _currentPopupList.Count == 0)
            {
                var currentElement = _queueDatas[0];
                if (!_currentPopupList.Contains(currentElement.WindowData.ID))
                {
                    _currentPopupList.Add(currentElement.WindowData.ID);
                    await _poolController.Show(currentElement.WindowData, currentElement.Parameters);
                }
            }
        }

        /// <summary>
        /// Release Window/Popup
        /// </summary>
        /// <param name="windowData"></param>
        public void Release(WindowData windowData)
        {
            if (windowData == null)
            {
                Debug.LogError("id is null or empty");
                return;
            }

            _poolController.Release(windowData);
        }

        public bool IsActive(WindowData windowData) 
        {
            return _poolController.IsActive(windowData);
        }

        public bool IsAnyPopupActive()
        {
            return _currentPopupList.Count > 0;
        }

        /// <summary>
        /// Check Escape
        /// </summary>
        public void Tick() 
        {
            if (Input.GetKeyDown(KeyCode.Escape)) 
            {
                _poolController.Escape();
            }

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                _poolController.Enter();
            }
        }
    }
}
