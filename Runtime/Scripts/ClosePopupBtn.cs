﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Cysharp.Threading.Tasks;

namespace UIWindowExtenject
{
    /// <summary>
    /// Close Popup Button
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class ClosePopupBtn : MonoBehaviour
    {
        [SerializeField]
        private WindowData _windowData;

        protected WindowController _windowController;

        [Inject]
        public void Construct(WindowController windowController)
        {
            _windowController = windowController;
        }

        private Button _button;

        private void OnEnable()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnPointerClick);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnPointerClick);
        }

        public void OnPointerClick()
        {
            _windowController.ClosePopup(_windowData).Forget();
        }
    }
}
