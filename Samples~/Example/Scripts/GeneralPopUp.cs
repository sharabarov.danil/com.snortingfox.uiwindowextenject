using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UIWindowExtenject 
{

    /// <summary>
    /// General pop up
    /// </summary>
    public class GeneralPopUp : MonoBehaviour, IShow, IHide 
    {

        [SerializeField]
        private TMP_Text _titleTxt;

        [SerializeField]
        private TMP_Text _descriptionTxt;

        [SerializeField]
        private ButtonData _closeBtnData;

        [SerializeField]
        private ButtonData _funcBtnData;

        private GeneralPopUpData _data;

        public void Hide()
        {
            if (_data.CloseBtnData != null)
            {
                _closeBtnData.ClickBtn.onClick.RemoveListener(OnClose);
            }

            if (_data.FuncBtnData != null)
            {
                _funcBtnData.ClickBtn.onClick.RemoveListener(OnFunc);
            }
        }

        public void Show(IParameters parameters) 
        {
            if (parameters is GeneralPopUpData)
            {
                _data = parameters as GeneralPopUpData;
            }

            if (_data == null) 
            {
                return;
            }

            _titleTxt.text = _data.Title;
            _descriptionTxt.text = _data.Description;

            _closeBtnData.ButtonObj.SetActive(_data.CloseBtnData != null);
            if (_data.CloseBtnData != null) 
            {
                _closeBtnData.TitleTxt.text = _data.CloseBtnData.Title;
                _closeBtnData.ClickBtn.onClick.AddListener(OnClose);
            }

            _funcBtnData.ButtonObj.SetActive(_data.FuncBtnData != null);
            if (_data.FuncBtnData != null) 
            {
                _funcBtnData.TitleTxt.text = _data.FuncBtnData.Title;
                _funcBtnData.ClickBtn.onClick.AddListener(OnFunc);
            }

        }

        private void OnClose() 
        {
            if (_data.CloseBtnData != null)
            {
                _data.CloseBtnData.OnClick?.Invoke();
            }
        }

        private void OnFunc()
        {
            if (_data.CloseBtnData != null)
            {
                _data.FuncBtnData.OnClick?.Invoke();
            }
        }



        [Serializable]
        public class ButtonData 
        {
            [SerializeField]
            private GameObject _buttonObj;
            [SerializeField]
            private TMP_Text _titleTxt;
            [SerializeField]
            private Button _clickBtn;

            public GameObject ButtonObj => _buttonObj;

            public TMP_Text TitleTxt => _titleTxt;

            public Button ClickBtn => _clickBtn;

        }
    }
}
