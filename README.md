# UIWindowExtenject
**The plugin for creation of the controller for working UI windows.**

This plugin allows to open and close windows/popups. In the plugin all window's/popup's prefabs are stored with using Addressables plugin.

Stack of technologies:
1. Zenject
2. Addressables
3. UniTask